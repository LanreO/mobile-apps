package com.graydientcreative.mobile.safehouse;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Environment;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.MediaController;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.VideoView;

import java.io.File;
import java.io.IOException;

public class MsMCamera extends AppCompatActivity implements SurfaceHolder.Callback  {

    VideoView videoView;

    private MediaPlayer mediaPlayer;
    private SurfaceHolder vidHolder;
    private SurfaceView vidSurface;

    String url = "https://portal.hdontap.com/s/embed/?stream=17th_delmar-mux_hdontap-HDOT";
    //String url = "https://portal.hdontap.com/s/embed/?stream=17th_delmar-mux_hdontap-HDOT";
    Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_ms_mcamera);

        Uri uri = Uri.parse(url);

        vidSurface = (SurfaceView)findViewById(R.id.surfView);
        vidHolder = vidSurface.getHolder();
        vidHolder.addCallback(this);
        //videoView =(VideoView)findViewById(R.id.videoView);
        mediaPlayer = new MediaPlayer();

/*
<VideoView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_below="@+id/headerCamera"
        android:layout_centerVertical="true"
        android:layout_centerHorizontal="true"
        android:id="@+id/videoView" />


MediaController mediacontroller = new MediaController(
                MsMCamera.this);
        videoView.setVideoPath(url);

        mediacontroller.setAnchorView(videoView);

        videoView.setMediaController(mediacontroller);

        videoView.start();

        try {
            // Start the MediaController
            MediaController mediacontroller = new MediaController(
                    MsMCamera.this);
            mediacontroller.setAnchorView(videoView);

            // Get the URL from String VideoURL
            videoView.setMediaController(mediacontroller);
            videoView.setVideoURI(uri);

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                videoView.start();
            }
        });



        videoView =(VideoView)findViewById(R.id.videoView);
            MediaController mediaController = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                mediaController = new MediaController(this);
                mediaController.setAnchorView(videoView);
                videoView.setMediaController(mediaController);

                videoView.setVideoURI(uri);
                videoView.start();
            }





        */
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mediaPlayer.setDisplay(holder);
        try {
            mediaPlayer.setDataSource(url);

            mediaPlayer.prepare();

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaPlayer.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }
}
