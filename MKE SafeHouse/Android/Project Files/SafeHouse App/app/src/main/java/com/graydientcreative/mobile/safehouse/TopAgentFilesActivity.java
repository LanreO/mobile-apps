package com.graydientcreative.mobile.safehouse;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class TopAgentFilesActivity extends AppCompatActivity {

    ImageButton btnBack;
    ListView topAgentList;
    TextView txtTopAgentsTitle;
    String[] agentNames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_agent_files);

        Typewriter writer = (Typewriter)findViewById(R.id.txtTopAgentsTitle);
        //Add a character every 150ms
        writer.setCharacterDelay(100);
        writer.animateText("Top Secret: Agent Files");

        Main();
    }

    protected void Main() {
        //SetBackButton();

        txtTopAgentsTitle = (TextView) findViewById(R.id.txtTopAgentsTitle);
        Typeface f25 = Typeface.createFromAsset(getAssets(), "f25_executive.otf");
        txtTopAgentsTitle.setTypeface(f25);

        agentNames = getResources().getStringArray(R.array.top_agent_names);
        ArrayList<String> nameList = new ArrayList<String>();

        for(String names : agentNames) {
            nameList.add(names);
        }

        topAgentList = (ListView) findViewById(R.id.topAgentList);
        topAgentList.setAdapter(new CustomAdapter(this, nameList));
        topAgentList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3)
            {
                String value = (String)adapter.getItemAtPosition(position);

                Intent intent = new Intent(TopAgentFilesActivity.this, TopAgentBioActivity.class);
                intent.putExtra("Agent Name", value);
                startActivity(intent);
            }
        });
    }

    /*protected void SetBackButton() {
        btnBack = (ImageButton) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TopAgentFilesActivity.this, FeatureListActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(TopAgentFilesActivity.this, FeatureListActivity.class);
        startActivity(intent);
        finish();
    }*/
}
