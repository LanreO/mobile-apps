package com.graydientcreative.mobile.safehouse;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.ActionBar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by LanreO on 10/3/2016.
 */

public class MissionAdapter extends BaseAdapter {

    Typeface firaSans;
    Context context;
    String[] data;
    int[] isChecked;
    private static LayoutInflater inflater = null;
    MissionAdapter.ChangeItemInterface changeItemInterface;

    Integer totalObjectiveCount = 0;
    Integer completedObjectiveCount = 0;

    public MissionAdapter(Context context, String[] data) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.changeItemInterface = (MissionAdapter.ChangeItemInterface) context;

        firaSans = Typeface.createFromAsset(context.getAssets(), "firasans-italic.otf");

        totalObjectiveCount = getCount();
        isChecked = new int[totalObjectiveCount];
        //Toast.makeText(context, String.valueOf(getCount()), Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data[position];
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;

        if (vi == null)
            vi = inflater.inflate(R.layout.custom_list_item, null);

        final ImageView icon = (ImageView) vi.findViewById(R.id.item_icon);
        RelativeLayout.LayoutParams iconParams = new RelativeLayout.LayoutParams(100, 100);
        iconParams.addRule(RelativeLayout.CENTER_VERTICAL);
        iconParams.leftMargin = 50;
        iconParams.rightMargin = 25;
        icon.setLayoutParams(iconParams);
        if(isChecked[position] == 1) {
            icon.setBackgroundResource(R.drawable.checked_box);
            icon.setTag("checked");
        } else{
            icon.setBackgroundResource(R.drawable.unchecked_box);
            icon.setTag("unchecked");
        }

        //icon.setTag("unchecked");

        vi.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(icon.getTag() == "unchecked") {
                    icon.setBackgroundResource(R.drawable.checked_box);
                    icon.setTag("checked");
                    completedObjectiveCount++;
                    isChecked[position] = 1;
                } else {
                    icon.setBackgroundResource(R.drawable.unchecked_box);
                    icon.setTag("unchecked");
                    completedObjectiveCount--;
                    isChecked[position] = 0;
                }
                changeItemInterface.doChange(completedObjectiveCount + " of 10 completed");

                if(completedObjectiveCount == totalObjectiveCount) {
                    Intent intent = new Intent(context, MissionCompleteActivity.class);
                    context.startActivity(intent);
                    ((Activity)context).finish();
                }
            }
        });

        TextView text = (TextView) vi.findViewById(R.id.item_title);

        RelativeLayout.LayoutParams textParams = new RelativeLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        //textParams.addRule(RelativeLayout.ALIGN_LEFT);
        //params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        textParams.addRule(RelativeLayout.RIGHT_OF, R.id.item_icon);
        textParams.rightMargin = 30;
        textParams.leftMargin = 15;

        text.setGravity(Gravity.LEFT);
        text.setTypeface(firaSans);
        text.setText(data[position]);
        text.setTextColor(Color.BLACK);
        text.setTextSize(16);
        text.setLayoutParams(textParams);
        return vi;
    }

    public interface ChangeItemInterface {

        public void doChange(String anyValue);

    }
}