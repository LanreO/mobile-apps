package com.graydientcreative.mobile.safehouse;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Calendar;

public class HoursActivity extends AppCompatActivity {

    ImageButton btnBack;
    TextView txtHoursTitle, txtCurrentDateHours, txtDate1, txtDate2, txtDate3, txtDate4, txtDate5, txtDate6;

    String[] hours = new String[] {
        "Sunday: 11:00 AM - 12:00 AM",
        "Monday: 11:00 AM - 12:00 AM",
        "Tuesday: 11:00 AM - 12:00 AM",
        "Wednesday: 11:00 AM - 1:00 AM",
        "Thursday: 11:00 AM - 12:00 AM",
        "Friday: 11:00 AM - 12:00 AM",
        "Saturday: 11:00 AM - 12:00 AM",
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hours);

        Typewriter writer = (Typewriter)findViewById(R.id.txtHoursTitle);
        //Add a character every 150ms
        writer.setCharacterDelay(100);
        writer.animateText("Hours");

        Main();
    }

    protected void Main() {
        /*btnBack = (ImageButton) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HoursActivity.this, RestaurantInfoListActivity.class);
                startActivity(intent);
                finish();
            }
        });*/

        txtHoursTitle = (TextView) findViewById(R.id.txtHoursTitle);
        Typeface f25 = Typeface.createFromAsset(getAssets(), "f25_executive.otf");
        txtHoursTitle.setTypeface(f25);

        txtCurrentDateHours = (TextView) findViewById(R.id.txtCurrentDateHours);
        Typeface firaSans = Typeface.createFromAsset(getAssets(), "firasans-italic.otf");
        txtCurrentDateHours.setTypeface(firaSans, Typeface.BOLD);

        int currentDay = GetDate() - 1;
        String currentDayText = "Open Today: " + hours[currentDay].substring(hours[currentDay].indexOf(':') + 2);

        txtCurrentDateHours.setText(currentDayText);

        int nextDate = currentDay + 1;

        txtDate1 = (TextView) findViewById(R.id.txtDate1);
        txtDate1.setTypeface(firaSans);
        txtDate1.setText(hours[nextDate]);
        nextDate = UpdateNextDate(nextDate);

        txtDate2 = (TextView) findViewById(R.id.txtDate2);
        txtDate2.setTypeface(firaSans);
        txtDate2.setText(hours[nextDate]);
        nextDate = UpdateNextDate(nextDate);

        txtDate3 = (TextView) findViewById(R.id.txtDate3);
        txtDate3.setTypeface(firaSans);
        txtDate3.setText(hours[nextDate]);
        nextDate = UpdateNextDate(nextDate);

        txtDate4 = (TextView) findViewById(R.id.txtDate4);
        txtDate4.setTypeface(firaSans);
        txtDate4.setText(hours[nextDate]);
        nextDate = UpdateNextDate(nextDate);

        txtDate5 = (TextView) findViewById(R.id.txtDate5);
        txtDate5.setTypeface(firaSans);
        txtDate5.setText(hours[nextDate]);
        nextDate = UpdateNextDate(nextDate);

        txtDate6 = (TextView) findViewById(R.id.txtDate6);
        txtDate6.setTypeface(firaSans);
        txtDate6.setText(hours[nextDate]);
    }

    protected int GetDate() {
        String strDayOfWeek = "";
        Calendar c = Calendar.getInstance();

        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

        return dayOfWeek;
    }

    protected int UpdateNextDate(int date) {
        int newDate = date + 1;

        if(newDate > 6) {
            newDate = 0;
        }

        return newDate;
    }
}
