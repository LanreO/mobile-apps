package com.graydientcreative.mobile.safehouse;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MissionCompleteActivity extends AppCompatActivity {

    ImageButton btnViewReward;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mission_complete);

        btnViewReward = (ImageButton) findViewById(R.id.btnViewReward);
        btnViewReward.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MissionCompleteActivity.this, RewardActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
