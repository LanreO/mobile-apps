package com.graydientcreative.mobile.safehouse;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.io.ByteArrayOutputStream;

public class TestCanvasActivity extends AppCompatActivity implements View.OnTouchListener {

    int btnDoneHeight, headerHeight, iconHeight;
    RelativeLayout header, rlTopParent;
    ImageButton btnBack, btnDone;
    ViewGroup imageGroup;
    ImageView imgHat, imgSunglasses, imgBowtie, imgMustache, imgUser, selectedImage;
    private int _xDelta;
    private int _yDelta;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int CROP_PIC = 2;

    ScaleGestureDetector SGD;
    int yValue = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_canvas);

        SGD = new ScaleGestureDetector(this, new ScaleListener());
        Main();
    }

    protected void Main() {
        header = (RelativeLayout) findViewById(R.id.headerMenu);
        rlTopParent = (RelativeLayout) findViewById(R.id.rlTopParent);
        btnBack = (ImageButton) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TestCanvasActivity.this, FeatureListActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnDone = (ImageButton) findViewById(R.id.btnDone);
        btnDoneHeight = btnDone.getMeasuredHeight();
        btnDone.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(TestCanvasActivity.this, FinishedImageActivity.class);
                //final View view = findViewById(R.id.imageView);
                //view.buildDrawingCache();
                //Bitmap image= view.getDrawingCache();
                //Bundle extras = new Bundle();
                //extras.putParcelable("imagebitmap", image);
                //intent.putExtras(extras);
                //startActivity(intent);
                //finish();


                Display display = getWindowManager().getDefaultDisplay();
                DisplayMetrics outMetrics = new DisplayMetrics();
                display.getMetrics(outMetrics);

                float density  = getResources().getDisplayMetrics().density;
                int dpHeight = (int) (outMetrics.heightPixels / density);
                int dpWidth  = (int) (outMetrics.widthPixels / density);

/*
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int width = size.x;
                width = width - 200;
                int height = size.y;
                height = height - 200;
                */
                int imageHeight, imageWidth, xValue, yValue;

                imageWidth = outMetrics.widthPixels;

                imageHeight = (int) (outMetrics.heightPixels - convertDpToPixel(350));

                System.out.println("X: " + String.valueOf(imgUser.getX()));
                System.out.println("Y: " + String.valueOf(imgUser.getY()));
                int dpY = (int) (imgUser.getY() * 2.925);
                System.out.println("Height: " + String.valueOf(imgUser.getHeight()));
                System.out.println("Width: " + String.valueOf(imgUser.getWidth()));
                //header.setVisibility(View.INVISIBLE);
                //btnDone.setVisibility(View.INVISIBLE);
                //height = height - 100;

                //Toast.makeText(getApplicationContext(), String.valueOf(btnDoneHeight), Toast.LENGTH_SHORT).show();
                //Toast.makeText(getApplicationContext(), String.valueOf(dpWidth), Toast.LENGTH_SHORT).show();

                View v1 = getWindow().getDecorView().getRootView();
                //View v1 = findViewById(R.id.imgUser);
                v1.setDrawingCacheEnabled(true);
                Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache(), 0, dpY, imgUser.getWidth(), imgUser.getHeight());
                v1.setDrawingCacheEnabled(false);

                //imgUser.setImageBitmap(bitmap);

                imgHat.setVisibility(View.GONE);
                imgBowtie.setVisibility(View.GONE);
                imgMustache.setVisibility(View.GONE);
                imgSunglasses.setVisibility(View.GONE);


                Intent intent = new Intent(TestCanvasActivity.this, FinishedImageActivity.class);
                ByteArrayOutputStream bStream = new ByteArrayOutputStream();
                //Compress it before sending it to minimize the size and quality of bitmap.
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, bStream);
                byte[] byteArray = bStream.toByteArray();
                intent.putExtra("image", byteArray);
                startActivity(intent);
                //rlTopParent.setBackgroundColor(Color.GREEN);
                //header.setVisibility(View.VISIBLE);
                //btnDone.setVisibility(View.VISIBLE);
            }
        });

        final RelativeLayout view_map = (RelativeLayout) findViewById(R.id.imageView);
        final View view = findViewById(R.id.imageView);

        SetImages();
        //yValue = header.getMeasuredHeight() + imgHat.getMeasuredHeight();
        //Toast.makeText(getApplicationContext(), String.valueOf(header.getMeasuredHeight()), Toast.LENGTH_SHORT).show();
        dispatchTakePictureIntent();
    }

    public float convertDpToPixel(float dp){
        Resources resources = getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    protected void SetImages() {
        imgHat = (ImageView) findViewById(R.id.imgHat);
        imgHat.setOnTouchListener(this);
        imgHat.bringToFront();

        imgSunglasses = (ImageView) findViewById(R.id.imgSunglasses);
        imgSunglasses.setOnTouchListener(this);
        imgSunglasses.bringToFront();

        imgBowtie = (ImageView) findViewById(R.id.imgBowtie);
        imgBowtie.setOnTouchListener(this);
        imgBowtie.bringToFront();

        imgMustache = (ImageView) findViewById(R.id.imgMustache);
        imgMustache.setOnTouchListener(this);
        imgMustache.bringToFront();

        imgUser = (ImageView) findViewById(R.id.imgUser);
    }

    public boolean onTouch(View view, MotionEvent event) {
        Integer viewId = view.getId();
        selectedImage = (ImageView) view;


        SGD.onTouchEvent(event);

        switch(viewId) {
            case 2131427442:
                //Toast.makeText(getApplicationContext(), "Sunglasses!", Toast.LENGTH_SHORT).show();
                break;
            case 2131427443:
                //Toast.makeText(getApplicationContext(), "Hat!", Toast.LENGTH_SHORT).show();
                break;
            case 2131427444:
                //Toast.makeText(getApplicationContext(), "Mustache!", Toast.LENGTH_SHORT).show();
                break;
            case 2131427445:
                //Toast.makeText(getApplicationContext(), "Bowtie!", Toast.LENGTH_SHORT).show();
                break;
        }

        final int X = (int) event.getRawX();
        final int Y = (int) event.getRawY();
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                _xDelta = X - lParams.leftMargin;
                _yDelta = Y - lParams.topMargin;
                break;
            case MotionEvent.ACTION_UP:
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
            case MotionEvent.ACTION_MOVE:
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                layoutParams.leftMargin = X - _xDelta;
                layoutParams.topMargin = Y - _yDelta;
                //layoutParams.rightMargin = -250;
                //layoutParams.bottomMargin = -250;
                view.setLayoutParams(layoutParams);
                break;
        }
        view.invalidate();
        return true;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("Got here!");
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imgUser.setImageBitmap(imageBitmap);
            //picUri = data.getData();
            //performCrop();
        } else if (requestCode == CROP_PIC) {
            if(data != null) {
                // get the returned data
                Bundle extras = data.getExtras();
                // get the cropped bitmap
                Bitmap thePic = extras.getParcelable("data");
                //ImageView picView = (ImageView) findViewById(R.id.imgUserPhoto);
                imgUser.setImageBitmap(thePic);
            } else {
                finish();
            }
        }
    }

    private class ScaleListener extends
            ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            float scaleFactor = detector.getScaleFactor();

            int width_in_px = (int) (selectedImage.getWidth() * scaleFactor);
            int height_in_px = (int) (selectedImage.getHeight() * scaleFactor);


            if(height_in_px >= 40 && width_in_px >= 90) {
                selectedImage.getLayoutParams().height = height_in_px;
                selectedImage.getLayoutParams().width = width_in_px;
                selectedImage.requestLayout();
            }

            return true;
        }
    }
}
