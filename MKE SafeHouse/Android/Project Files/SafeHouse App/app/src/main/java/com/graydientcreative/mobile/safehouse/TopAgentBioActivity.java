package com.graydientcreative.mobile.safehouse;

import android.content.res.Resources;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TopAgentBioActivity extends AppCompatActivity {

    ImageView agentImage;
    ImageButton btnBack;
    TextView txtAgentNumber, txtAgentNumberLabel, txtAgentName,
            txtAgentAlias, txtAgentAliasLabel, txtAgentAffiliation,
            txtAgentAffiliationLabel, txtLastKnownLocation, txtLastKnownLocationLabel,
            txtSkillOne, txtSkillTwo, txtSkillThree , txtSkillsLabel;

    String agentName = "", strAgentNumber = "";
    Boolean twoAgents = false, flag = true;
    int height = 0, width = 0, agentNumber = 0;
    String[] agentNames, agentAliases, agentAffiliations, lastKnownLocations, skillOne, skillTwo, skillThree;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_agent_bio);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras != null) {
                agentName = extras.getString("Agent Name");
            }
        }
        if(agentName.contains("Charlie Sheen")) {
            twoAgents = true;
        }

        Main();
    }

    protected void Main() {
        //SetBackButton();

        InstantiateViews();
        InstantiateArrays();

        SetAgentInfo();
    }

    protected void SetAgentInfo() {
        Typeface firaSans = Typeface.createFromAsset(getAssets(), "firasans-italic.otf");

        for(int i = 0; i < agentNames.length; i++) {
            if(agentNames[i].toLowerCase().contains(agentName.toLowerCase())){
                txtAgentName.setText(agentName);
                txtAgentAlias.setText(agentAliases[i]);
                txtAgentAlias.setTypeface(firaSans);
                txtAgentAffiliation.setText(agentAffiliations[i]);
                txtAgentAffiliation.setTypeface(firaSans);
                txtLastKnownLocation.setText(lastKnownLocations[i]);
                txtLastKnownLocation.setTypeface(firaSans);
                txtSkillOne.setText(skillOne[i]);
                txtSkillOne.setTypeface(firaSans);
                txtSkillTwo.setText(skillTwo[i]);
                txtSkillTwo.setTypeface(firaSans);
                txtSkillThree.setText(skillThree[i]);
                txtSkillThree.setTypeface(firaSans);

                agentNumber = i + 1;
            }
        }

        switch(agentName) {
            case "Greg Marcus":
                height = 700;
                width = 500;
                agentImage.setBackgroundResource(R.drawable.greg_marcus);
                break;
            case "David Gruber":
                height = 600;
                width = 800;
                agentImage.setBackgroundResource(R.drawable.david_gruber);
                break;
            default:
                Resources r = getResources();
                float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 325, r.getDisplayMetrics());
                width = (int) px;

                px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 275, r.getDisplayMetrics());
                height = (int) px;
                flag = false;
                break;
        }

        if(flag) {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, height);
            params.addRule(RelativeLayout.BELOW, R.id.txtAgentName);
            params.addRule(RelativeLayout.CENTER_HORIZONTAL, 1);

            agentImage.setLayoutParams(params);
        }

        if(agentNumber < 10) {
            strAgentNumber = "100" + String.valueOf(agentNumber);
        } else {
            strAgentNumber = "10" + String.valueOf(agentNumber);
        }
        if(twoAgents) {
            strAgentNumber = strAgentNumber + ", 1026";
        }
        txtAgentNumber.setText(strAgentNumber);
    }

    protected void InstantiateArrays() {
        agentNames = getResources().getStringArray(R.array.top_agent_names);
        agentAliases = getResources().getStringArray(R.array.top_agent_aliases);
        agentAffiliations = getResources().getStringArray(R.array.top_agent_affiliations);
        lastKnownLocations = getResources().getStringArray(R.array.last_known_locations);
        skillOne = getResources().getStringArray(R.array.agent_skill_one);
        skillTwo = getResources().getStringArray(R.array.agent_skill_two);
        skillThree = getResources().getStringArray(R.array.agent_skill_three);
    }

    protected void InstantiateViews() {
        agentImage = (ImageView) findViewById(R.id.imgAgent);

        txtAgentNumber = (TextView) findViewById(R.id.txtAgentNumber);
        txtAgentNumberLabel = (TextView) findViewById(R.id.txtAgentNumberLabel);
        Typeface f25 = Typeface.createFromAsset(getAssets(), "f25_executive.otf");
        txtAgentNumberLabel.setTypeface(f25);

        txtAgentName = (TextView) findViewById(R.id.txtAgentName);
        txtAgentName.setTypeface(f25);

        txtAgentAlias = (TextView) findViewById(R.id.txtAgentAlias);
        txtAgentAliasLabel = (TextView) findViewById(R.id.txtAgentAliasLabel);
        txtAgentAliasLabel.setTypeface(f25);

        txtAgentAffiliation = (TextView) findViewById(R.id.txtAgentAffiliation);
        txtAgentAffiliationLabel = (TextView) findViewById(R.id.txtAgentAffiliationLabel);
        txtAgentAffiliationLabel.setTypeface(f25);

        txtLastKnownLocation = (TextView) findViewById(R.id.txtLastKnownLocation);
        txtLastKnownLocationLabel = (TextView) findViewById(R.id.txtLastKnownLocationLabel);
        txtLastKnownLocationLabel.setTypeface(f25);

        txtSkillsLabel = (TextView) findViewById(R.id.txtSkillsLabel);
        txtSkillsLabel.setTypeface(f25);
        txtSkillOne = (TextView) findViewById(R.id.txtSkillOne);
        txtSkillTwo = (TextView) findViewById(R.id.txtSkillTwo);
        txtSkillThree = (TextView) findViewById(R.id.txtSkillThree);

        if(twoAgents) {
            txtAgentNumberLabel.setText("Numbers:");
            txtAgentAliasLabel.setText("Aliases:");
            txtAgentAffiliationLabel.setText("Affiliations:");
        }
    }

    /*protected void SetBackButton() {
        btnBack = (ImageButton) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }*/
}
