package com.graydientcreative.mobile.safehouse;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class FingerPrintActivity extends AppCompatActivity {

    Boolean isScanning = false;
    int scanHeight;
    ImageButton btnBack;
    ImageView imgScanner, imgFingerprintScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_finger_print);

        Main();
    }

    protected void Main() {
        //SetBackButton();

        int screenHeight = getApplicationContext().getResources().getDisplayMetrics().heightPixels;
        scanHeight = screenHeight - 450;
        //Toast.makeText(getApplicationContext(), "Screen Height: " + screenHeight, Toast.LENGTH_SHORT).show();

        imgScanner = (ImageView) findViewById(R.id.imgScanner);
        //int scannerHeight = imgScanner.getHeight();
        //Toast.makeText(getApplicationContext(), "Scanner Height: " + scannerHeight, Toast.LENGTH_SHORT).show();

        imgFingerprintScreen = (ImageView) findViewById(R.id.imgFingerprintScreen);
        imgFingerprintScreen.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                FingerprintFunction();

            }
        });

        imgFingerprintScreen.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                FingerprintFunction();
                return false;
            }
        });
    }

    protected void FingerprintFunction() {
        if(!isScanning) {
            isScanning = true;

            new CountDownTimer(1250, 28) {
                int counter = 0;

                public void onTick(long millisUntilFinished) {
                    //Toast.makeText(getApplicationContext(), "seconds remaining: " + millisUntilFinished / 1000, Toast.LENGTH_SHORT).show();

                    counter = counter + 45;
                    if (counter < scanHeight) {
                        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                        params.setMargins(0, 0, 0, counter);
                        imgScanner.setLayoutParams(params);
                    }
                }

                public void onFinish() {

                    new CountDownTimer(1500, 28) {
                        int counter = 125;

                        public void onTick(long millisUntilFinished) {
                            //Toast.makeText(getApplicationContext(), "seconds remaining: " + millisUntilFinished / 1000, Toast.LENGTH_SHORT).show();
                            counter = counter + 45;
                            if (counter < scanHeight + 75) {
                                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                                params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                                params.setMargins(0, counter, 0, 0);
                                imgScanner.setLayoutParams(params);
                            }
                        }

                        public void onFinish() {
                            //Toast.makeText(getApplicationContext(), "Done!", Toast.LENGTH_SHORT).show();
                            //GoToMainListActivity();
                            isScanning = false;
                            GoToTopAgentFilesActivity();
                        }
                    }.start();

                }
            }.start();
        }
    }

    protected void GoToTopAgentFilesActivity() {
        Intent intent = new Intent(FingerPrintActivity.this, TopAgentFilesActivity.class);
        startActivity(intent);
        finish();
    }

    /*protected void SetBackButton() {
        btnBack = (ImageButton) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FingerPrintActivity.this, FeatureListActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(FingerPrintActivity.this, FeatureListActivity.class);
        startActivity(intent);
        finish();
    }*/
}
