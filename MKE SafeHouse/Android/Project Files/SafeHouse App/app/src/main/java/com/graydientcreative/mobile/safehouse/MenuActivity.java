package com.graydientcreative.mobile.safehouse;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;

public class MenuActivity extends AppCompatActivity {

    WebView myWebView;
    ImageButton btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Main();
    }

    protected void Main() {

        /*btnBack = (ImageButton) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, RestaurantInfoListActivity.class);
                startActivity(intent);
                finish();
            }
        });*/

        myWebView = (WebView) findViewById(R.id.webView);
        myWebView.getSettings().setJavaScriptEnabled(true);

        String html ="<html><body ><div id=\"menusContainer\"></div>\n" +
                "<script type=\"text/javascript\" src=\"https://menus.singleplatform.co/businesses/storefront/?apiKey=ke09z8icq4xu8uiiccighy1bw\">\n" +
                "</script>\n" +
                "<script>\n" +
                "var options = {};\n" +
                "options['PrimaryBackgroundColor'] = '#fff';\n" +
                "options['MenuDescBackgroundColor'] = '#fff';\n" +
                "options['SectionTitleBackgroundColor'] = '#000';\n" +
                "options['SectionDescBackgroundColor'] = '#000';\n" +
                "options['ItemBackgroundColor'] = '#fff';\n" +
                "options['PrimaryFontFamily'] = 'Calibri';\n" +
                "options['BaseFontSize'] = '15px';\n" +
                "options['FontCasing'] = 'Default';\n" +
                "options['PrimaryFontColor'] = '#000000';\n" +
                "options['MenuDescFontColor'] = '#000000';\n" +
                "options['SectionTitleFontColor'] = '#faf9f9';\n" +
                "options['SectionDescFontColor'] = '#000';\n" +
                "options['ItemTitleFontColor'] = '#de2227';\n" +
                "options['FeedbackFontColor'] = '#fafafa';\n" +
                "options['ItemDescFontColor'] = '#000';\n" +
                "options['ItemPriceFontColor'] = '#fafafa';\n" +
                "options['HideDisplayOptionPhotos'] = 'true';\n" +
                "options['HideDisplayOptionDollarSign'] = 'true';\n" +
                "options['HideDisplayOptionPrice'] = 'true';\n" +
                "options['HideDisplayOptionDisclaimer'] = 'true';\n" +
                "options['HideDisplayOptionAttribution'] = 'true';\n" +
                "options['MenuTemplate'] = '2';\n" +
                "options['MenuIframe'] = 'true';\n" +
                "new BusinessView(\"safehouse-2\", \"menusContainer\", options);\n" +
                "</script></body></html>";
        String mime = "text/html";
        String encoding = "utf-8";

        myWebView.loadDataWithBaseURL(null, html, mime, encoding, null);
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (Uri.parse(url).getHost().equals("http://www.safe-house.com/")) {
                // This is my web site, so do not override; let my WebView load the page
                return false;
            }
            // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
            return true;
        }
    }
}
