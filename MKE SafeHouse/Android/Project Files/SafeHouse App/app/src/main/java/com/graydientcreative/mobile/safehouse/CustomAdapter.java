package com.graydientcreative.mobile.safehouse;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.ActionBar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by LanreO on 9/19/2016.
 */
public class CustomAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> data;
    Boolean flag = false;
    private int mShortAnimationDuration;
    private int mAgentNameDelay = 0;

    private static LayoutInflater inflater = null;

    public CustomAdapter(Context context, ArrayList<String> data) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public CustomAdapter(Context context, ArrayList<String> data, Boolean flag) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.flag = flag;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        Typeface f25 = Typeface.createFromAsset(context.getAssets(), "f25_executive.otf");

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        params.addRule(RelativeLayout.CENTER_VERTICAL);
        params.addRule(RelativeLayout.RIGHT_OF, R.id.item_icon);

        if (vi == null) {
            vi = inflater.inflate(R.layout.custom_list_item, null);
        }

        TextView text = (TextView) vi.findViewById(R.id.item_title);
        ImageView icon = (ImageView) vi.findViewById(R.id.item_icon);

        // Retrieve and cache the system's default "short" animation time.
        mShortAnimationDuration = 1000;

        int mShortAnimationDelay = 0;


        text.setTextSize(20);
        switch(data.get(position)) {
            case "AGENT MISSIONS":
                if(flag) {
                    vi.setAlpha(0f);
                    vi.setVisibility(View.VISIBLE);
                }

                icon.setBackgroundResource(R.drawable.agent_missions_icon);

                params.leftMargin = 100;
                icon.getLayoutParams().height = 200;
                icon.getLayoutParams().width = 175;
                mShortAnimationDelay = 500;
                break;
            case "VILLAN/ SPY SELFIE":
                if(flag) {
                    vi.setAlpha(0f);
                    vi.setVisibility(View.VISIBLE);
                }

                params.leftMargin = 45;
                icon.setBackgroundResource(R.drawable.selfie_icon);
                icon.getLayoutParams().height = 225;
                icon.getLayoutParams().width = 225;
                break;
            case "TOP AGENT FILES":
                if(flag) {
                    vi.setAlpha(0f);
                    vi.setVisibility(View.VISIBLE);
                }

                params.leftMargin = 45;
                icon.setBackgroundResource(R.drawable.top_secret_icon);
                icon.getLayoutParams().height = 200;
                icon.getLayoutParams().width = 225;
                mShortAnimationDelay = 1000;
                break;
            case "SAFEHOUSE INTEL":
                if(flag) {
                    vi.setAlpha(0f);
                    vi.setVisibility(View.VISIBLE);
                }

                params.leftMargin = 75;
                icon.setBackgroundResource(R.drawable.info_icon);
                icon.getLayoutParams().height = 200;
                icon.getLayoutParams().width = 200;
                mShortAnimationDelay = 1500;
                break;

            case "MS M CAMERA VIEW":
                if(flag) {
                    vi.setAlpha(0f);
                    vi.setVisibility(View.VISIBLE);
                }

                params.leftMargin = 75;
                icon.setBackgroundResource(R.drawable.camera_icon);
                icon.getLayoutParams().height = 200;
                icon.getLayoutParams().width = 200;
                break;
            case "MY REWARD":
                if(flag) {
                    vi.setAlpha(0f);
                    vi.setVisibility(View.VISIBLE);
                }
                params.leftMargin = 75;
                icon.setBackgroundResource(R.drawable.code_icon);
                icon.getLayoutParams().height = 200;
                icon.getLayoutParams().width = 200;
                mShortAnimationDelay = 2000;
                break;
            case "HOURS":
                params.leftMargin = 75;
                icon.setBackgroundResource(R.drawable.hours_icon);
                icon.getLayoutParams().height = 175;
                icon.getLayoutParams().width = 175;
                //text.setTextSize(17);
                break;
            case "MENU":
                params.leftMargin = 80;
                icon.setBackgroundResource(R.drawable.food_icon);
                icon.getLayoutParams().height = 225;
                icon.getLayoutParams().width = 175;
                break;
            default:
                params.leftMargin = 50;
                icon.setBackgroundResource(R.drawable.folder_icon);
                icon.getLayoutParams().height = 175;
                icon.getLayoutParams().width = 150;
                text.setTextSize(18);
                break;
        }

        text.setTextSize(17);
        params.topMargin = 50;
        text.setGravity(Gravity.LEFT);
        text.setText(data.get(position));
        text.setTextColor(Color.BLACK);
        text.setLayoutParams(params);
        text.setTypeface(f25);

        if(flag) {
            vi.animate()
                    .setStartDelay(mShortAnimationDelay)
                    .alpha(1f)
                    .setDuration(mShortAnimationDuration)
                    .setListener(null);
        }

        return vi;
    }
}
