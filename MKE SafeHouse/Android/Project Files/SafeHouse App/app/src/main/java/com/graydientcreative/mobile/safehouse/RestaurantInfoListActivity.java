package com.graydientcreative.mobile.safehouse;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class RestaurantInfoListActivity extends AppCompatActivity {

    ImageButton btnBack;
    ListView listView ;
    ArrayList<String> options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restraunt_info_list);

        Main();
    }

    protected void Main() {

        /*btnBack = (ImageButton) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(RestaurantInfoListActivity.this, FeatureListActivity.class);
                startActivity(intent);
                finish();
            }
        });*/

        options = new ArrayList<String>();
        options.add("HOURS");

        if(isNetworkAvailable()) {
            options.add("MENU");
        }

        // Get ListView object from xml
        listView = (ListView) findViewById(R.id.listRestaurantInfo);
        listView.setAdapter(new CustomAdapter(this, options, false));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3)
            {
                String value = (String)adapter.getItemAtPosition(position);

                switch(value) {
                    case "HOURS":
                        GoToHours();
                        break;
                    case "MENU":
                        GoToMenu();
                        break;
                }
                // assuming string and if you want to get the value on click of list item
                // do what you intend to do on click of listview row
            }


        });
    }

    protected void GoToMenu() {
        Intent intent = new Intent(RestaurantInfoListActivity.this, MenuActivity.class);
        startActivity(intent);
    }

    protected void GoToHours() {
        Intent intent = new Intent(RestaurantInfoListActivity.this, HoursActivity.class);
        startActivity(intent);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /*@Override
    public void onBackPressed() {
        Intent intent = new Intent(RestaurantInfoListActivity.this, FeatureListActivity.class);
        startActivity(intent);
        finish();
    }*/
}
