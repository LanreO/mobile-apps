package com.graydientcreative.mobile.safehouse;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class FeatureListActivity extends Activity {

    SharedPreferences prefs;
    ListView listView;
    Boolean fadeIn = false;
    String[] availableFeatures;
    CustomAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feature_list);

        Main();
    }

    private void Main() {
        ArrayList<String> options = GenerateOptionsArray();

        adapter = new CustomAdapter(this, options, fadeIn);
        // Get ListView object from xml
        listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(adapter);
        fadeIn = false;
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3)
            {
                String value = (String)adapter.getItemAtPosition(position);

                switch(value) {
                    case "SAFEHOUSE INTEL":
                        GoToRestaurantInformation();
                        break;
                    case "VILLAN/ SPY SELFIE":
                        GoToTestCanvasActivity();
                        break;
                    case "AGENT MISSIONS":
                        GoToAgentMissions();
                        break;
                    case "Finger Print Test":
                        GoToFingerprintActivity();
                        break;
                    case "TOP AGENT FILES":
                        GoToFingerprintActivity();
                        break;
                    case "MY REWARD":
                        GoToRewardActivity();
                        break;
                    case "MS M CAMERA VIEW":
                        GoToActivity(MsMCamera.class);
                        break;
                }
            }
        });

    }

    private ArrayList<String> GenerateOptionsArray() {
        ArrayList<String> options = new ArrayList<String>();
        availableFeatures = getResources().getStringArray(R.array.available_features);

        for(String feature : availableFeatures) {
            options.add(feature);
        }

        if(CheckCouponExpiration()) {
            options.add("MY REWARD");
        }

        return options;
    }

    protected boolean CheckCouponExpiration() {
        Boolean result = false;
        Calendar today = Calendar.getInstance();

        prefs = getSharedPreferences("Coupons", MODE_PRIVATE);
        String date = prefs.getString("Expiration Date", null);

        if(date != "" && date != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
            Calendar storedDate = today;

            try {
                storedDate.setTime(sdf.parse(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            long storedTime = storedDate.getTimeInMillis();

            //Calendar currentDate = today;
            long currentTime = today.getTimeInMillis();

            if (storedTime < currentTime) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("Coupon Code", "");
                editor.putString("Expiration Date", "");
                editor.apply();
            } else {
                result = true;
            }
        }
        return result;
    }

    protected void GoToActivity(Class c) {
        Intent intent = new Intent(FeatureListActivity.this, c);
        startActivity(intent);
    }

    protected void GoToSelfieActivity() {
        Intent intent = new Intent(FeatureListActivity.this, PhotoActivity.class);
        startActivity(intent);
        finish();
    }

    protected void GoToRewardActivity() {
        Intent intent = new Intent(FeatureListActivity.this, RewardActivity.class);
        startActivity(intent);
    }

    protected void GoToTestCanvasActivity() {
        Intent intent = new Intent(FeatureListActivity.this, TestCanvasActivity.class);
        startActivity(intent);
    }

    protected void GoToFingerprintActivity() {
        Intent intent = new Intent(FeatureListActivity.this, FingerPrintActivity.class);
        startActivity(intent);
    }

    protected void GoToRestaurantInformation() {
        Intent intent = new Intent(FeatureListActivity.this, RestaurantInfoListActivity.class);
        startActivity(intent);
    }
    protected void GoToAgentMissions() {
        Intent intent = new Intent(FeatureListActivity.this, AgentMissionActivity.class);
        startActivity(intent);
        finish();
    }
}