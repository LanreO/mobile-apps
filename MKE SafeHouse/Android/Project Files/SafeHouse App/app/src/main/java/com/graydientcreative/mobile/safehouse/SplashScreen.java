package com.graydientcreative.mobile.safehouse;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new CountDownTimer(2000, 1000) {

            public void onTick(long millisUntilFinished) {
                //Toast.makeText(getApplicationContext(), "seconds remaining: " + millisUntilFinished / 1000, Toast.LENGTH_SHORT).show();
            }

            public void onFinish() {
                //Toast.makeText(getApplicationContext(), "Done!", Toast.LENGTH_SHORT).show();
                GoToMainListActivity();
            }
        }.start();
    }

    public void GoToMainListActivity() {
        Intent intent = new Intent(SplashScreen.this, FeatureListActivity.class);
        startActivity(intent);
        finish();
    }
}
