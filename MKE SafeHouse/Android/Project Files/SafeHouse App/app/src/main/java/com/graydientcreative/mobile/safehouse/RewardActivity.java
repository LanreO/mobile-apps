package com.graydientcreative.mobile.safehouse;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

public class RewardActivity extends AppCompatActivity {

    TextView txtHeader, txtCouponCode, txtExpirationDate;
    ImageButton btnBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward);

        /*Bundle extras = getIntent().getExtras();

        if (extras != null) {
            goToMainScreen = extras.getBoolean("Go To Home");
        }*/

        Typewriter writer = (Typewriter)findViewById(R.id.txtRewardHeader);
        //Add a character every 150ms
        writer.setCharacterDelay(100);
        writer.animateText("Rewards");

        /*btnBack = (ImageButton) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
            Intent intent = new Intent(RewardActivity.this, FeatureListActivity.class);
            startActivity(intent);
            finish();
            }
        });*/

        txtHeader = (TextView) findViewById(R.id.txtRewardHeader);
        Typeface f25 = Typeface.createFromAsset(getAssets(), "f25_executive.otf");
        txtHeader.setTypeface(f25);

        SharedPreferences sharedpreferences = getSharedPreferences("Coupons", Context.MODE_PRIVATE);
        String storedCode = sharedpreferences.getString("Coupon Code", null);
        String storedDate = sharedpreferences.getString("Expiration Date", null);

        txtExpirationDate = (TextView) findViewById(R.id.txtExpirationDate);
        txtCouponCode = (TextView) findViewById(R.id.txtCouponCode);

        if((storedCode == "" && storedDate == "") || storedCode == null || storedDate == null) {
            SharedPreferences.Editor editor = sharedpreferences.edit();

            String generatedDate = GetDate();
            String generatedCode = CreateCouponCode();

            editor.putString("Coupon Code", generatedCode);
            editor.putString("Expiration Date", generatedDate);
            editor.commit();

            String strExpirationDate = "Expiration Date: " + generatedDate;
            String strCouponCode = "Code " + generatedCode;

            txtExpirationDate.setText(strExpirationDate);
            txtCouponCode.setText(strCouponCode);

        } else {
            String strExpirationDate = "Expiration Date: " + storedDate;
            String strCouponCode = "Code " + storedCode;

            txtExpirationDate.setText(strExpirationDate);

            txtCouponCode.setText(strCouponCode);
        }
    }

    private String CreateCouponCode() {
        String finalCode = "007";

        Random r = new Random();
        int intCode = r.nextInt(9999 - 1000) + 1000;

    finalCode = String.valueOf(intCode);
    //Toast.makeText(getApplicationContext(), finalCode, Toast.LENGTH_SHORT).show();
    return finalCode;
}

    protected String GetDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 1);
        String strDate = sdf.format(c.getTime());
        return strDate;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RewardActivity.this, FeatureListActivity.class);
        startActivity(intent);
        finish();
    }
}
