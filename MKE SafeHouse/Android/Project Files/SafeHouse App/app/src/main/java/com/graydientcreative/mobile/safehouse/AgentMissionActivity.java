package com.graydientcreative.mobile.safehouse;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Random;

public class AgentMissionActivity extends AppCompatActivity implements MissionAdapter.ChangeItemInterface {

    TextView txtMissionTitle, txtMissionSubTitle, txtCompletedMissions;
    ImageButton btnBack;
    ListView missionList;
    String[] missionObjectives;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_mission);

        missionObjectives = getResources().getStringArray(R.array.missions);

        Typewriter writer = (Typewriter)findViewById(R.id.txtMissionTitle);
        //Add a character every 150ms
        writer.setCharacterDelay(100);
        writer.animateText("Your Secret Mission");

        Main();
    }

    protected void Main() {
        //SetBackButton();

        txtMissionTitle = (TextView) findViewById(R.id.txtMissionTitle);
        Typeface f25 = Typeface.createFromAsset(getAssets(), "f25_executive.otf");
        txtMissionTitle.setTypeface(f25);

        txtMissionSubTitle = (TextView) findViewById(R.id.txtMissionSubTitle);
        Typeface firaSans = Typeface.createFromAsset(getAssets(), "firasans-italic.otf");
        txtMissionSubTitle.setTypeface(firaSans);

        txtCompletedMissions = (TextView) findViewById(R.id.txtCompletedMissions);
        txtMissionTitle.setTypeface(f25);

        SetMissions();
    }

    protected void SetMissions() {
        String[] tenMissions = new String[10];
        String missionNumbers = "";
        int totalMissionCount = missionObjectives.length;
        Random r = new Random();

        for(int i = 0; i < tenMissions.length; i++) {

            int number = r.nextInt(totalMissionCount - 0);
            while(missionNumbers.contains(String.valueOf(number))){
                number = r.nextInt(totalMissionCount - 0);
            }
            missionNumbers = missionNumbers + String.valueOf(number) + ", ";
            tenMissions[i] = missionObjectives[number];
        }

        missionList = (ListView) findViewById(R.id.missionList);
        missionList.setAdapter(new MissionAdapter(this, tenMissions));
    }

    @Override
    public void doChange(String anyValue) {
        //Toast.makeText(this, anyValue, Toast.LENGTH_SHORT).show();
        txtCompletedMissions.setText(anyValue);
        /// Here you can update any value in your activity !
    }
    /*protected void SetBackButton() {
        btnBack = (ImageButton) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AgentMissionActivity.this, FeatureListActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }*/

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(AgentMissionActivity.this, FeatureListActivity.class);
        startActivity(intent);
        finish();
    }
}