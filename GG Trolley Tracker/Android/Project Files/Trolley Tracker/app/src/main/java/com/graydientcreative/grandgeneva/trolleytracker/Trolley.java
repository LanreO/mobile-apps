package com.graydientcreative.grandgeneva.trolleytracker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by LanreO on 10/24/2016.
 */

public class Trolley {

    public int id, status;
    public LatLng coordinates;
    public String driverName, trolleyName;
    public Bitmap icon;

    //constructor
    public Trolley(Context context, int id, int status, String driverName, double latitude, double longitude) {
        this.id = id;
        this.status = status;
        this.driverName = driverName;
        if(this.id == 1) {
            trolleyName = "Marc";
            this.icon = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.yellow_trolley), 75, 75, true);
        } else {
            trolleyName = "GG";
            this.icon = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.blue_trolley), 75, 75, true);
        }
        this.coordinates = new LatLng(latitude, longitude);
    }

}
