package com.graydientcreative.grandgeneva.trolleytracker;

import android.os.AsyncTask;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class MyAsyncTask extends AsyncTask<String, Integer, String> {
    String errorMessage;
    Integer Action;
    String Result = null;
    String temp = null;

    protected String doInBackground(String... params) {
        if(params[0].contains("&")){
            Action = Integer.parseInt(params[0].substring(7, params[0].indexOf('&')));
            temp = params[0].substring(params[0].indexOf('&') + 1);
        } else {
            Action = Integer.parseInt(params[0].substring(7));
        }
        String url = "";
        switch (Action) {
            //Get information about all the stops
            case 1:
                //url = "http://ggt-dev.marcusapps.com/api/stops";
                url = "https://api.marcusapps.com/ggt/api/stops";
                break;
            //Get information about all the trolleys
            case 2:
                //url = "http://ggt-dev.marcusapps.com/api/drivers";
                url = "https://api.marcusapps.com/ggt/api/drivers";
                break;
            case 3:
                //url = "http://ggt-dev.marcusapps.com/api/requests";
                url = "https://api.marcusapps.com/ggt/api/stops";
                break;
            default:
                Result = "Invalid action entry";
                break;
        }

        if (url.length() > 0) {
            System.out.println("Sending to: " + url);
            if(temp == null){
                Result = GetData(url);
            } else {
                //System.out.println("Got to POST");
                Result = GetData(url, temp);
            }
        }
        //System.out.println("Result: " + Result);
        return Result;
    }

    protected void onProgressUpdate(Integer... progress) {
        int testsSoFar = progress[0];
        int totalTests = progress[1];
    }

    protected String onPostExecute(Integer result) {
        return Result;
    }

    protected String GetData(String strURL){
        Integer result = null;
        try {
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            try {

                URL url = new URL(strURL);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setDoInput(true);

                String line;
                StringBuffer buffer = new StringBuffer();
                reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

                Integer responseCode = urlConnection.getResponseCode();

                if(responseCode == HttpURLConnection.HTTP_OK){
                    while ((line = reader.readLine()) != null) {
                        buffer.append(line + "\n");
                        //System.out.println(line);
                    }
                    reader.close();
                    Result = buffer.toString();
                } else {
                    Result = "Response code: " + String.valueOf(responseCode);
                }

            } catch (Exception e) {
                System.out.println("Error: " + e.toString());
                errorMessage = e.toString();
                return errorMessage;
            }
        }catch (Exception e) {
            System.out.println("Error: " + e.toString());
            errorMessage = e.toString();
            return errorMessage;
        }

        return Result;
    }

    protected String GetData(String strURL, String parameters) {
        Integer result = null;
        try {
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            try {

                URL url = new URL(strURL);
                urlConnection = (HttpURLConnection) url.openConnection();

                JSONObject jsonObj;
                if(Action == 99){
                    urlConnection.setRequestMethod("PUT");
                    jsonObj = SetDriverLocationJson(parameters);
                } else{
                    urlConnection.setRequestMethod("POST");
                    jsonObj = SetJson(parameters);
                }
                String strJSON = jsonObj.toString();
                strJSON = strJSON.replace("\"","\\\"");

                System.out.println("Sending this: " + strJSON);

                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);

                OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream());
                writer.write(jsonObj.toString());
                writer.flush();

                Integer responseCode = urlConnection.getResponseCode();
                System.out.println("Response code: " + urlConnection.getResponseCode());
                if(responseCode == HttpURLConnection.HTTP_OK){
                    String line;
                    StringBuffer buffer = new StringBuffer();
                    reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

                    while ((line = reader.readLine()) != null) {
                        buffer.append(line + "\n");
                        //System.out.println(line);
                    }
                    writer.close();
                    reader.close();
                    Result = buffer.toString();
                } else {
                    Result = "Response code: " + String.valueOf(responseCode);
                }

            } catch (Exception e) {
                System.out.println("Error: " + e.toString());
                errorMessage = e.toString();
                return errorMessage;
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error" + e.toString());
            errorMessage = "Error" + e.toString();
            return errorMessage;
        }
        return Result;
    }

    public JSONObject SetDriverLocationJson(String parameters) {
        JSONObject jsonobj = new JSONObject();

        try{
            while(parameters.contains("&")){
                String name = parameters.substring(0,parameters.indexOf('='));
                String object = parameters.substring(parameters.indexOf('=') + 1, parameters.indexOf('&'));

                if(name.contains("current")) {
                    Double temp = Double.parseDouble(object);
                    jsonobj.put(name, temp);
                } else if(name.contains("Id")) {
                    Integer temp = Integer.parseInt(object);
                    jsonobj.put(name, temp);
                } else {
                    jsonobj.put(name, object);
                }

                parameters = parameters.substring(parameters.indexOf('&') + 1);
            }

            jsonobj.put("isSignedIn", true);

            String name = parameters.substring(0,parameters.indexOf('='));
            String object = parameters.substring(parameters.indexOf('=') + 1);
            jsonobj.put(name, object);

        } catch(Exception e){
            e.printStackTrace();
            System.out.println("Error converting string to JSON. " + e.toString());
            errorMessage = "Error" + e.toString();
        }
        return jsonobj;
    }

    private JSONObject SetJson(String parameters) {
        JSONObject jsonobj = new JSONObject();
        try{
            while(parameters.contains("&")){
                String name = parameters.substring(0,parameters.indexOf('='));
                System.out.println(name);
                String object = parameters.substring(parameters.indexOf('=') + 1, parameters.indexOf('&'));
                if((name.equals("status") || name.contains("Id")) && !name.equals("deviceId")) {
                    Integer temp = Integer.parseInt(object);
                    jsonobj.put(name, temp);
                } else {
                    jsonobj.put(name, object);
                }
                parameters = parameters.substring(parameters.indexOf('&') + 1);
            }

            String name = parameters.substring(0,parameters.indexOf('='));
            String object = parameters.substring(parameters.indexOf('=') + 1);
            jsonobj.put(name, object);

        } catch(Exception e){
            e.printStackTrace();
            System.out.println("Error converting string to JSON. " + e.toString());
            errorMessage = "Error" + e.toString();
        }
        return jsonobj;
    }
}
