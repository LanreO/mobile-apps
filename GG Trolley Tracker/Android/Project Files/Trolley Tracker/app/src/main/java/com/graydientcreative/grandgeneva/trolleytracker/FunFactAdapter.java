package com.graydientcreative.grandgeneva.trolleytracker;

import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by LanreO on 9/26/2016.
 */

public class FunFactAdapter extends BaseAdapter {

    private Context context;
    private String[] data;
    private int counter = 0;
    private static LayoutInflater inflater = null;

    public FunFactAdapter(Context context, String[] data) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data[position];
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;

        if (vi == null)
            vi = inflater.inflate(R.layout.custom_list_item_1, null);

        SetFactText(vi, position);

        int bgColor = context.getResources().getColor(R.color.accentListBackground);

        if((counter % 2) != 0) {
            vi.setBackgroundColor(bgColor);
        }
        counter++;

        return vi;
    }

    protected void SetFactText(View view, int position) {
        TextView text = (TextView) view.findViewById(R.id.item_text);

        SpannableStringBuilder sb = new SpannableStringBuilder(data[position]);
        StyleSpan b = new StyleSpan(android.graphics.Typeface.BOLD);
        sb.setSpan(b, 0, data[position].indexOf(':') + 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        text.setText(sb);
    }
}
