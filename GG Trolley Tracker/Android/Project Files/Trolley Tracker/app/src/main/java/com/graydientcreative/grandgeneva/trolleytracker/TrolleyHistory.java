package com.graydientcreative.grandgeneva.trolleytracker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class TrolleyHistory extends AppCompatActivity {

    ImageButton btnClose;
    TextView trolleyName, txtDetailLabel, txtOtherLabel;
    String[] trolleyNameArray = new String[] { "Marc", "GG" };

    String[] detailsTextArray = new String[] {
            "Details: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut molestie lacus eu dui blandit, nec scelerisque est cursus." +
                    " Donec posuere, dolor at vulputate scelerisque, metus est rutrum augue, in lacinia dui elit ut leo.",
            "Details: Phasellus euismod rutrum nunc eu aliquet. Proin elementum eros elit, imperdiet gravida augue facilisis eget. Suspendisse" +
                    " consequat diam nec sem hendrerit cursus. Maecenas ultrices ultrices orci."
    };

    String[] otherTextArray = new String[] {
            "Other: Sed nisl sapien, posuere id pharetra sed, commodo rhoncus justo. Curabitur urna diam, venenatis sed molestie sit amet, pharetra ac tellus.",
            "Other: Donec tincidunt auctor nisi, sed varius ante mollis blandit. Aenean convallis bibendum dolor, eu dapibus nisl auctor eget."
    };

    int selectedTrolley = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trolley_history);

        trolleyName = (TextView) findViewById(R.id.txtTrolleyName);
        txtDetailLabel = (TextView) findViewById(R.id.txtDetailLabel);
        txtOtherLabel = (TextView) findViewById(R.id.txtOtherLabel);

        SetBackButton();

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int screenWidth = (int) (dm.widthPixels * 0.9);
        int screenHeight = (int) (dm.heightPixels * 0.85);

        getWindow().setLayout(screenWidth, screenHeight);

        try {
            Intent mIntent = getIntent();
            selectedTrolley = mIntent.getIntExtra("trolleyNumber", 0);

        } catch (Exception e) {

        }

        if(selectedTrolley != 0) {
            trolleyName.setText(trolleyNameArray[selectedTrolley - 1]);

            SetDetailLabelText();
            SetOtherLabelText();
        }
    }

    protected void SetDetailLabelText() {
        SpannableStringBuilder sb = new SpannableStringBuilder(detailsTextArray[selectedTrolley - 1]);
        StyleSpan b = new StyleSpan(android.graphics.Typeface.BOLD);
        sb.setSpan(b, 0, 8, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        txtDetailLabel.setText(sb);
    }

    protected void SetOtherLabelText() {
        SpannableStringBuilder sb = new SpannableStringBuilder(otherTextArray[selectedTrolley - 1]);
        StyleSpan b = new StyleSpan(android.graphics.Typeface.BOLD);
        sb.setSpan(b, 0, 6, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        txtOtherLabel.setText(sb);
    }

    protected void SetBackButton() {
        btnClose = (ImageButton) findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
