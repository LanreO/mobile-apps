package com.graydientcreative.grandgeneva.trolleytracker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class FunFacts extends AppCompatActivity {

    Spinner stopSpinner;
    ListView factList;
    ImageView btnDropDown;
    TextView txtBack;
    String[] stopNames; // = new String[] { "GG Main Entrance", "Wellspa", "Golf Shop" }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fun_facts);

        if(savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras != null) {
                try {
                    stopNames = extras.getStringArray("Stop Array");
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Error getting stop names.", Toast.LENGTH_SHORT).show();
                }
            }
        }


        factList = (ListView) findViewById(R.id.factList);
        stopSpinner = (Spinner) findViewById(R.id.stopSpinner);
        btnDropDown = (ImageView) findViewById(R.id.imgDropdown);
        btnDropDown.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                stopSpinner.performClick();
            }
        });

        txtBack = (TextView) findViewById(R.id.txtBack);
        txtBack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, stopNames);
        stopSpinner.setAdapter(adapter);

        String[] factArray = new String[] {
                "Fact #1: Aenean massa ex, hendrerit sit amet magna a, semper viverra nisl. Pellentesque augue ex, finibus non vehicula vitae, pharetra ut ipsum.",
                "Fact #2: Duis elementum, metus nec fringilla consectetur, leo nibh ornare ex, viverra porta nulla sem id augue. Morbi in lectus pretium, fringilla risus nec, egestas lacus.",
                "Fact #3: Fusce eu ultrices erat. Mauris vehicula purus vitae convallis ornare.",
                "Fact #4: Donec a condimentum ante, sit amet rutrum augue. Curabitur pellentesque, dolor non convallis suscipit, sapien dui gravida mi, a tincidunt enim turpis nec erat.",
                "Fact #5: Nam dignissim quam vel lectus facilisis, vitae interdum magna tincidunt."
        };

        factList.setAdapter(new FunFactAdapter(this, factArray));
    }
}
