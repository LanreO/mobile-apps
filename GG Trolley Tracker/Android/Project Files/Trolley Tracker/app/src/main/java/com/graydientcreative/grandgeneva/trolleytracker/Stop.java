package com.graydientcreative.grandgeneva.trolleytracker;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by LanreO on 10/24/2016.
 */

public class Stop {

    public int id;
    public LatLng coordinates;
    public String name;
    public Boolean isDisabled;
    public Bitmap icon;

    //constructor
    public Stop(int id, String name, double latitude, double longitude, String iconURL, Boolean isDisabled) {
        this.id = id;
        this.name = name;
        this.coordinates = new LatLng(latitude, longitude);
        this.isDisabled = isDisabled;
        try {
            DownloadImageTask task = new DownloadImageTask();
            icon = Bitmap.createScaledBitmap(task.execute(iconURL).get(), 75, 75, true);
            task.cancel(true);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage().toString());
            //Toast.makeText(getApplicationContext(), "Error attempting to get stop locations.", Toast.LENGTH_SHORT).show();
        }
    }
}
