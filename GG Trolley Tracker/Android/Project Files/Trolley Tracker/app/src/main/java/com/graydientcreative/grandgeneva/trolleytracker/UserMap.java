package com.graydientcreative.grandgeneva.trolleytracker;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class UserMap extends FragmentActivity implements OnMapReadyCallback {

    //************************ Variables section ************************************
    private GoogleMap mMap;
    private Marker mMyMarker;
    private LatLng myLocation;
    ArrayList<Marker> markers;
    Marker marc, gg;

    Intent historyIntent;

    SharedPreferences prefs;
    Integer requestStopId = 0;
    Button btnRequestRide;
    String requestStopName = "";
    Boolean canRequestRide = true;

    StopArrayList stops;
    ArrayList<Trolley> activeTrolleys;

    TextView txtPrivacyPolicy;

    Timer timer;

    RelativeLayout back_dim_layout;
    Boolean justOpened = true;

    private LocationManager locationManager;
    private android.location.LocationListener locationListener;
    String[] locationProviders = new String[] { LocationManager.GPS_PROVIDER, LocationManager.NETWORK_PROVIDER };
    //************************ Variables section ************************************

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_map_container);

        markers = new ArrayList<>();
        back_dim_layout = (RelativeLayout) findViewById(R.id.bac_dim_layout);
        stops = new StopArrayList();
        prefs = getSharedPreferences("Device Identifier", MODE_PRIVATE);
        historyIntent = new Intent(UserMap.this, TrolleyHistory.class);

        SetButtons();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.userMap);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // How close the map allows you to zoom in
        mMap.setMaxZoomPreference(18);

        // How far the map allows you to zoom out
        mMap.setMinZoomPreference(14);

        mMap.getUiSettings().setRotateGesturesEnabled(false);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setTiltGesturesEnabled(false);

        try {
            boolean success = mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle( this, R.raw.style_json));

            if (!success) {
                Log.e("MapsActivityRaw", "Style parsing failed.");
            }
        } catch(Exception e) {
            Log.e("MapsActivityRaw", "Can't find style.", e);
        }

        // (Min Latitude, Min Longitude), (Max Latitude, Max Longitude)
        LatLngBounds boundaries = new LatLngBounds(new LatLng(42.59728, -88.4116), new LatLng(42.6121, -88.3863473));
        mMap.setLatLngBoundsForCameraTarget(boundaries);
        mMap.setTrafficEnabled(false);

        LatLng GGMainEntrance = new LatLng(42.61015, -88.4054);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(GGMainEntrance, 15);
        mMap.animateCamera(cameraUpdate);

        /*mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener(){

            @Override
            public boolean onMarkerClick(Marker marker) {
                if (marker.getTitle().contains("Marc"))
                {
                    back_dim_layout.setVisibility(View.VISIBLE);
                    historyIntent.putExtra("trolleyNumber", 1);
                    startActivity(historyIntent);
                } else if (marker.getTitle().equals("GG"))
                {
                    back_dim_layout.setVisibility(View.VISIBLE);
                    historyIntent.putExtra("trolleyNumber", 2);
                    startActivity(historyIntent);
                } else if (!marker.getTitle().equals("My Location")){
                    String selectedStop, temp;
                    selectedStop = marker.getTitle();
                    String[] stopNameArray = new String[stopArray.length()];
                    stopNameArray[0] = selectedStop;
                    for(int i = 1; i < stopArray.length(); i++) {
                        try {
                            JSONObject obj = stopArray.getJSONObject(i);
                            if(obj.getString("name") != selectedStop) {
                                stopNameArray[i] = obj.getString("name");
                            }
                        } catch(Exception e) {
                            Toast.makeText(getApplicationContext(), "Error getting stop name.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    Intent factIntent = new Intent(UserMap.this, FunFacts.class);
                    factIntent.putExtra("Stop Array", stopNameArray);
                    startActivity(factIntent);
                }
                    return false;
                }
            });*/

        Main();
    }

    protected void Main() {
        HandleStops();
        HandleTrolleys();

        // Create and start timer to get location info from the API
        timer = new Timer();
        timer.scheduleAtFixedRate( new TimerClass(), 0, 5000);

        // Check permissions to get the user's location
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            SetLocationManager();

            if(locationManager.isProviderEnabled(locationProviders[0])) {
                Location location = locationManager.getLastKnownLocation(locationProviders[0]);
                if(location != null) {
                    UpdateUserLocation(location);
                }
            } else if(locationManager.isProviderEnabled(locationProviders[1])) {
                Location location = locationManager.getLastKnownLocation(locationProviders[1]);
                if(location != null) {
                    UpdateUserLocation(location);
                }
            }
        }
    }

    //************************ User Location section ************************************
    private void SetLocationManager() {
        locationListener = new MyLocationListener();
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            // getting GPS status
            Boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            if (isGPSEnabled) {
                //System.out.println("The location manager is using the phone's GPS.");
                locationManager.requestLocationUpdates(locationProviders[0], 0, 0, locationListener);
            } else {
                //System.out.println("The location manager is using the phone's network access.");
                locationManager.requestLocationUpdates(locationProviders[1], 0, 0, locationListener);
            }
        }
    }

    public void UpdateUserLocation(Location location) {
        LatLng stopLocation = null;
        int stopId = 0;
        String stopName = "";
        Boolean isNearAStop = false;
        float[] results = new float[1];

        myLocation = new LatLng(location.getLatitude(), location.getLongitude());
        if (mMyMarker == null) {
            mMyMarker = mMap.addMarker(new MarkerOptions().position(myLocation).title("My Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.person_icon)));
            markers.add(mMyMarker);
        } else {
            mMyMarker.setPosition(myLocation);
        }

        Location.distanceBetween(myLocation.latitude, myLocation.longitude, stops.enabledStops().get(0).coordinates.latitude, stops.enabledStops().get(0).coordinates.longitude, results);
        if(!(results[0] < 300) && justOpened) {
            justOpened = false;
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(stops.enabledStops().get(0).coordinates, 15);
            mMap.animateCamera(cameraUpdate);
        }

        /*if(stopArray != null) {
            for (int i = 0; i < stopArray.length(); i++) {
                try {
                    JSONObject stopObject = stopArray.getJSONObject(i);
                    stopName = stopObject.getString("name");
                    stopId = stopObject.getInt("id");

                    Double latitude = stopObject.getDouble("latitude");
                    Double longitude = stopObject.getDouble("longitude");

                    stopLocation = new LatLng(latitude, longitude);

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Error checking stop locations.", Toast.LENGTH_SHORT).show();
                }
                Location.distanceBetween(myLocation.latitude, myLocation.longitude, stopLocation.latitude, stopLocation.longitude, results);
                if(results[0] < 150) {
                    isNearAStop = true;

                    if(canRequestRide) {
                        String strResult = "You are " + String.valueOf(results[0]) + " meters from the " + stopName;// + String.valueOf(id);
                        requestStopId = stopId;
                        requestStopName = stopName;
                        //System.out.println(strResult);
                        //Toast.makeText(getApplicationContext(), strResult, Toast.LENGTH_SHORT).show();
                        btnRequestRide.setVisibility(View.VISIBLE);
                    }
                }

            }
            if(!isNearAStop) {
                //startActivity(new Intent(MapsActivity.this, Popup.class));
                if(requestStopId != 0) {
                    requestStopId = 0;
                }
                if(requestStopName != "") {
                    requestStopName = "";
                }
                Toast.makeText(getApplicationContext(), "You're not near any stops.", Toast.LENGTH_SHORT).show();
            }

        } else {
            //Toast.makeText(getApplicationContext(), "Stop array was empty.", Toast.LENGTH_SHORT).show();
        }*/

    }
    //************************ User Location section ************************************

    //************************ Stop section ************************************

    private void HandleStops() {
        GetStops();
        AddStopsToMap();
    }

    private void GetStops() {
        MyAsyncTask task = new MyAsyncTask();
        String result = "";
        String parameters = "action=1";

        try{
            result = task.execute(parameters).get();
            task.cancel(true);

            JSONArray temp = new JSONArray(result);
            SetStopObjects(temp);
        } catch (Exception e) {
            //Toast.makeText(getApplicationContext(), "There was an error attempting to get the stops.", Toast.LENGTH_SHORT).show();
        }
    }

    private void SetStopObjects(JSONArray stopJSONArray) throws JSONException {
        for(int i = 0; i < stopJSONArray.length(); i++) {
            JSONObject stopObject = stopJSONArray.getJSONObject(i);

            int id = stopObject.getInt("id");
            String name = stopObject.getString("name");
            Double latitude = stopObject.getDouble("latitude");
            Double longitude = stopObject.getDouble("longitude");
            String iconURL = stopObject.getString("iconUrl");
            Boolean isDisabled = stopObject.getBoolean("isDisabled");

            Stop newStop = new Stop(id, name, latitude, longitude, iconURL, isDisabled);
            stops.add(newStop);
        }
    }

    public void AddStopsToMap(){
        for(Stop stop : stops.enabledStops()) {
            Marker obj = mMap.addMarker(new MarkerOptions().position(stop.coordinates).title(stop.name).icon(BitmapDescriptorFactory.fromBitmap(stop.icon)));
            markers.add(obj);
        }
    }
    //************************ Stop section ************************************

    //************************ Trolley section ************************************

    private void HandleTrolleys() {
        GetTrolleys();
        AddTrolleysToMap();
    }

    private void GetTrolleys() {
        MyAsyncTask task = new MyAsyncTask();
        String result = "";
        String parameters = "action=2";

        try{
            result = task.execute(parameters).get();
            task.cancel(true);

            JSONArray temp = new JSONArray(result);
            SetTrolleyArray(temp);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.getMessage().toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private void SetTrolleyArray(JSONArray trolleys) throws JSONException  {
        Boolean marcIsActive = false;
        Boolean ggIsActive = false;
        int status = 0;
        activeTrolleys = new ArrayList<>();

        for(int i = 0; i < trolleys.length(); i++) {
            JSONObject trolleyObject = trolleys.getJSONObject(i);

            int id = trolleyObject.getInt("currentTrolleyId");
            Boolean isSignedIn = trolleyObject.getBoolean("isSignedIn");
            //status = trolleyObject.getInt("signInStatus");

            if(status == 1 || isSignedIn) {
                String driverName = trolleyObject.getString("name");
                switch(id) {
                    case 1:
                        marcIsActive = true;
                        break;
                    case 2:
                        ggIsActive = true;
                        break;
                }

                Double latitude = trolleyObject.getDouble("currentLatitude");
                Double longitude = trolleyObject.getDouble("currentLongitude");

                Trolley newTrolley = new Trolley(this, id, 1, driverName, latitude, longitude);
                activeTrolleys.add(newTrolley);

                //Toast.makeText(getApplicationContext(), "Got here.", Toast.LENGTH_SHORT).show();
            }
        }

        if(!marcIsActive && marc != null) {
            marc.remove();
            marc = null;
        }

        if(!ggIsActive && gg != null) {
            gg.remove();
            gg = null;
        }
    }

    private void AddTrolleysToMap() {
        for(Trolley trolley : activeTrolleys) {
            switch(trolley.id) {
                case 1:
                    if(marc == null) {
                        marc = mMap.addMarker(new MarkerOptions().position(trolley.coordinates).title(trolley.trolleyName).icon(BitmapDescriptorFactory.fromBitmap(trolley.icon)));
                        markers.add(marc);
                    } else {
                        marc.setPosition(trolley.coordinates);
                    }
                    break;
                case 2:
                    if(gg == null) {
                        gg = mMap.addMarker(new MarkerOptions().position(trolley.coordinates).title(trolley.trolleyName).icon(BitmapDescriptorFactory.fromBitmap(trolley.icon)));
                        markers.add(gg);
                    } else {
                        gg.setPosition(trolley.coordinates);
                    }
                    break;
            }
        }
    }
    //************************ Trolley section ************************************
/*
    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
        //Toast.makeText(getApplicationContext(), "Pausing location updates.", Toast.LENGTH_SHORT).show();
        // Release the Camera because we don't need it when paused
        // and other activities might need to use it.
        if (locationManager != null) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
            locationManager.removeUpdates(locationListener);
            locationManager = null;
        }
        if(timer != null) {
            timer.cancel();
        }
    }
*/
    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

        back_dim_layout.setVisibility(View.GONE);
    }

    private void SetButtons() {
        // Set Request-A-Ride button
        btnRequestRide = (Button) findViewById(R.id.btnRequestRide);
        btnRequestRide.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                RequestRide();
            }
        });

        // Set privacy policy button
        txtPrivacyPolicy = (TextView) findViewById(R.id.txtPrivacyPolicy);
        txtPrivacyPolicy.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                back_dim_layout.setVisibility(View.VISIBLE);
                Intent intent = new Intent(UserMap.this, PrivacyPolicy.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 21: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Main();
                } else {
                    finish();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    //************************ Location Listener section ************************************
    private final class MyLocationListener implements android.location.LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            // called when the listener is notified with a location update from the GPS
            if(location != null) {
                //System.out.println("My location changed! " + String.valueOf(location.getLatitude()) + ", " + String.valueOf(location.getLongitude()));
                UpdateUserLocation(location);
            }
        }

        public void onProviderDisabled(String provider) {
            // called when the GPS provider is turned off (user turning off the GPS on the phone)
        }

        public void onProviderEnabled(String provider) {
            // called when the GPS provider is turned on (user turning on the GPS on the phone)
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            // called when the status of the GPS provider changes
        }
    }
    //************************ Location Listener section ************************************

    //************************ Update Trolley Location section ************************************
    public class TimerClass extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (Looper.myLooper() == null) {
                        Looper.prepare();
                    }

                    HandleTrolleys();

                }
            });
        }
    }
    //************************ Update Trolley Location section ************************************

    protected void RequestRide() {
        MyAsyncTask task = new MyAsyncTask();
        String result = "";
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
        Calendar c = Calendar.getInstance();

        String deviceId = prefs.getString("Device ID", null);
        String strDate = sdf.format(c.getTime());

        if(deviceId != null) {
            Toast.makeText(getApplicationContext(), "Device " + deviceId
                    + " requested a ride at stop " + requestStopId + ", " + requestStopName
                    + " at " + strDate, Toast.LENGTH_LONG).show();
        }

        String parameters = "action=3&id=0&deviceId=" + deviceId +"&stopId=" + requestStopId + "&status=1&createdOn=" + strDate;

        try{
            result = task.execute(parameters).get();
            task.cancel(true);

            System.out.println("Result: " + result);
            if(result.contains("403")) {
                btnRequestRide.setVisibility(View.GONE);
                canRequestRide = false;

                back_dim_layout.setVisibility(View.VISIBLE);
                Intent intent = new Intent(UserMap.this, RequestResult.class);
                startActivity(intent);
            }
        } catch (Exception e) {
            //Toast.makeText(getApplicationContext(), "There was an error attempting to get the stops.", Toast.LENGTH_SHORT).show();
        }
    }
}
