package com.graydientcreative.grandgeneva.trolleytracker;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.view.View;
import android.widget.Toast;

import java.util.UUID;

public class SplashScreen extends AppCompatActivity {

    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        Main();
    }

    protected void Main() {
        prefs = getSharedPreferences("Device Identifier", MODE_PRIVATE);
        String deviceId = prefs.getString("Device ID", null);

        if(deviceId == null) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("Device ID", UUID.randomUUID().toString());
            editor.commit();
        }
    }

    private void ShowConnectivityPopup() {
        final Intent intent = new Intent();
        intent.setAction(Settings.ACTION_WIFI_SETTINGS);
        //Uri uri = Uri.fromParts("package", getPackageName(), null);
        //intent.setData(uri);

        AlertDialog alertDialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AppCompatAlertDialogStyle)).create();
        alertDialog.setTitle("Network Connection Error");
        alertDialog.setMessage("This application requires access to the internet.");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "SETTINGS",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startActivity(intent);
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        alertDialog.show();

    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

        new CountDownTimer(1000, 1000) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                if(isNetworkAvailable()) {
                    GoToMainActivity();
                } else {
                    ShowConnectivityPopup();
                }
            }
        }.start();
    }

    public void GoToMainActivity() {
        Intent intent = new Intent(SplashScreen.this, UserMap.class);
        startActivity(intent);
        finish();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
