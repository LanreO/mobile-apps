package com.graydientcreative.grandgeneva.trolleytracker;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.widget.TextView;

public class PrivacyPolicy extends AppCompatActivity {

    String policyText = "Grand Geneva Resort & Spa and Marcus Hotels and Resorts (“us,” “we” or “our”) created this Privacy Policy as a demonstration " +
            "of our commitment to the privacy of our online customers (“you” or “your”); it describes the information we may collect " +
            "through your use of our websites or through our mobile applications (together, “Services”) and how that information may be used.<br/><br/><b>Information Collected</b><br/><br/>" +
            "So that we can provide you a better customer experience, Marcus Hotels and Resorts may gather, retain and/or utilize personal information about you" +
            " during the course of providing Services, products, and information. This information is collected when you provide it voluntarily through" +
            " the Services, including by registering for certain promotions, contests or sweepstakes, or when you use one of our mobile applications.<br/><br/>" +
            "Your personally identifiable information (such as your name, address, telephone number, social security number, billing and shipping information, credit information, location, or e-mail address) will not be collected " +
            "website through your use of the Services unless you provide it voluntarily. That information will be used only for purposes for which it was collected, such as sending visitors information about our company and products." +
            " It also allows us to better tailor content to visitors’ needs and have a better overall understanding of the customers sites using our Services.<br/><br/>" +
            "When you use the Services, click stream and other usage data may be collected and analyzed at an aggregate level to aid in understanding how users navigate the Services, trends, and needs; none of this information will be reviewed at an individual level.<br/>" +
            "When you use the Services through one of our mobile applications, we may collect precise location data (“Location Data”) as part of the functionality of the Services. We collect Location Data through the Services so we can offer you certain location-based services and conduct analytics to improve the Services. For example, when you use the Services to view the location of a trolley or to request a trolley for a future pickup and you have enabled geolocation tracking, we will collect your Location Data to accurately depict your location in relation to the trolley.<br/><br/>" +
            "We collect Location Data in several ways from your wireless carrier and/or directly from the device on which you use the Services. If you are accessing the Services through one of our mobile applications, the way we collect Location Data will differ depending on your mobile device’s operating system. In all events, we do not collect Location Data, unless you have “allowed” its collection. If you decline to allow Location Data collection in the app, we will not collect your Location Data unless you manually enter it in. Further, while we may collect your Location Data, we are not able to determine your specific geolocation through the Services.<br/>" +
            "<br/><br/><b>Tracking Information</b><br/><br/>" +
            "Anonymous information may be collected through the use of various technologies, including cookies. For example, we might measure visitor volume on our websites, but do so in ways that keep the information anonymous. This anonymous data, also known as “clickstream data”, may be used to analyze trends and statistics so that we can provide a better customer experience.<br/>" +
            "<br/>Cookies are small files that a website installs on your hard drive. Cookies enable our web server to recognize your computer, which allows our websites to tailor content to better match your interests and preferences. Information gathered through cookies and web server logs may include the date and time of visits, the pages viewed, time spent at our website and the websites visited just before and just after our website. You can set your browser to notify you before you receive a cookie, giving you the chance to decide whether to accept it. You can also set your browser to block cookies altogether. However, if you do so, your ability to use this website may be impaired.<br/>" +
            "<br/><br/><b>Use and Sharing of Information</b><br/><br/>" +
            "We do not give or sell your personally identifiable information to third party organizations without your consent. We may share your information with agents and/or contractors of Marcus Hotels and Resorts in connection with services they perform that enable us to provide better online services to you. As an example, personal information may be used for purposes of administering our business activities, providing customer service, and making available other services and products to our customers and prospective customers. Our agents and contracts are required to maintain your information in the strictest confidence. We may store and process personally identifiable information to better understand your needs and how we may improve our services and products. Your information may be used to contact you in the future about important changes, such as about our websites, new services and products, promotions, sweepstakes and special offers. However, we never share your Location Data, which is retained only during your current session within our mobile applications or in order to fulfill your request for a future trolley pickup.<br/>" +
            "<br/><br/><b>Correcting, Updating or Deleting Your Information</b><br/><br/>" +
            "If you would like to correct, update, or delete personal information retained by us, or inform us of your desire not to receive future communications from us, please call (414) 905-1000.<br/>" +
            "<br/><br/><b>Confidentiality and Security</b><br/><br/>" +
            "Marcus Hotels and Resorts will take reasonable precautions to protect your personally identifiable information from loss, theft, misuse or alteration. Credit card information collected through the Services " +
            "is protected by secure server software which encrypts data sent via the internet. This information is hashed, or scrambled, en route and decoded once it reaches our website. Other e-mail that you send to us may not be secure unless we advise you that security measures will be in place prior to your transmitting the information. For that reason, we ask that you do not send your social security or credit card numbers to us through an unsecured email.<br/><br/>" +
            "Except as otherwise provided in this Privacy Policy, we will keep your personally identifiable information private and will not share it with third parties, unless such disclosure is necessary to: (a) comply with a court order or other legal process; (b) protect our rights or property; or (c) to enforce our Terms of Use.<br/>" +
            "<br/><br/><b>Minors</b><br/><br/>" +
            "We do not seek to obtain nor do we wish to receive personally identifiable information directly from minors. If a minor provides us with personally identifiable information, we encourage a parent or guardian of that child to contact us to have this information removed from our records and to unsubscribe the minor from future communication from us.<br/>" +
            "<br/><br/><b>Links to Third Party Websites</b><br/><br/>" +
            "The Services may contain links to other websites. These links are provided to you only as a convenience, and the inclusion of any links does not imply endorsement of the linked website(s) by Marcus Hotels and Resorts. Marcus Hotels and Resorts is not responsible for and does not warrant the suitability, accuracy, privacy practices or content of such websites.<br/>" +
            "<br/><br/><b>Your Consent</b><br/><br/>" +
            "By using the Services, you consent to our collection and use of your personally identifiable information as described in this Privacy Policy. Marcus Hotels and Resorts reserves the right to change or update this Privacy Policy, or any other policy or practice of Marcus Hotels and Resorts, at any time. Changes to our online privacy policies and procedures will be posted on our website to keep you aware of what information we collect, how we use it and under what circumstances we may disclose it. Any changes or updates will be effective immediately upon posting to the website. Thank you for visiting us. We value your patronage.<br/>" +
            "<br/><br/><b>About Marcus Hotels and Resorts</b><br/><br/>" +
            "Grand Geneva, LLC, an affiliate of Marcus Hotels and Resorts, owns and operates Grand Geneva.<br/>" +
            "Marcus Hotels and Resorts is a trade name describing the subsidiary companies of Marcus Hotels, Inc. that are involved in the hotel business in the United States. Marcus Hotels, Inc. is a wholly owned subsidiary of The Marcus Corporation, an NYSE corporation.<br/>" +
            "If you have any questions or concerns regarding our Privacy Policy, please contact us.<br/><br/><br/><br/>";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int screenWidth = (int) (dm.widthPixels * 0.8);
        int screenHeight = (int) (dm.heightPixels * 0.75);

        getWindow().setLayout(screenWidth, screenHeight);

        TextView privacyPolicyText = (TextView) findViewById(R.id.txtPrivacyPolicyText);

        privacyPolicyText.setText(Html.fromHtml(policyText));
    }
}
