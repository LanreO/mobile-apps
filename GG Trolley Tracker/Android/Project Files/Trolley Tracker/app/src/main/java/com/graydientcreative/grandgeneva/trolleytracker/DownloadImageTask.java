package com.graydientcreative.grandgeneva.trolleytracker;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    String errorMessage;
    Bitmap bitmapIcon = null;
    InputStream is;
    HttpURLConnection conn;

    public DownloadImageTask() {

    }

    protected void onPreExecute() {

    }

    protected Bitmap doInBackground(String... strURL) {
        try {
            URL url = new URL(strURL[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true);
            conn.connect();
            is = conn.getInputStream();
            bitmapIcon = BitmapFactory.decodeStream(is);
            is.close();
        } catch(Exception e) {
            System.out.println("Error: " + "Image download error");
            System.out.println("Error: " + e.getMessage());
        } finally
        {
            conn.disconnect();
        }
        return bitmapIcon;
    }

    protected void onPostExecute(Bitmap result) {

    }
}
