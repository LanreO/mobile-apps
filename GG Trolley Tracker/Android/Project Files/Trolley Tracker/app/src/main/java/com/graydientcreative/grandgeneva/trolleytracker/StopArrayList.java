package com.graydientcreative.grandgeneva.trolleytracker;

import java.util.ArrayList;

/**
 * Created by LanreO on 10/24/2016.
 */

public class StopArrayList {

    private ArrayList<Stop> stops;

    //constructor
    public StopArrayList() {
        stops = new ArrayList<>();
    }

    public void add(Stop newStop) {
        stops.add(newStop);
    }

    public void remove(Stop newStop) {
        stops.remove(newStop);
    }

    public ArrayList<Stop> enabledStops() {
        ArrayList<Stop> enabledStops = new ArrayList<>();

        for(Stop stop : stops) {
            if(!stop.isDisabled){
                enabledStops.add(stop);
            }
        }

        return enabledStops;
    }
}
