package com.graydientcreative.grandgeneva.trolleytracker;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.ImageView;

public class RequestResult extends AppCompatActivity {

    ImageView imgRequestBackground;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_result);

        imgRequestBackground = (ImageView) findViewById(R.id.imgRequestBackground);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int screenWidth = (int) (dm.widthPixels * 0.7);
        int screenHeight = (int) (dm.heightPixels * 0.5);

        getWindow().setLayout(screenWidth, screenHeight);
    }
}
